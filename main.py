from datetime import timedelta
from typing import Annotated

import uvicorn
from fastapi import FastAPI, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

import crud
import models
import schemas
import auth
from database import engine, get_db

models.Base.metadata.create_all(bind=engine)
app = FastAPI()



@app.post('/users/', response_model=schemas.User) #создание пользователя
def create_user(user:schemas.UserCreate, db:Session = Depends(get_db)):
    db_user = crud.get_username(db, username=user.username)
    if db_user:
        raise HTTPException(400,'Пользователь сцществует')
    else:
        return crud.create_user(db, user)

@app.post('/salary/add')
def create_salary(salary: schemas.SalaryCreate, db:Session = Depends(get_db)):
    crud.create_salary(db, salary)



@app.post("/token", response_model=schemas.Token)
def login_for_access_token(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()], db: Session = Depends(get_db)):
    user = auth.authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=auth.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = auth.create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return schemas.Token(access_token=access_token, token_type="bearer")

@app.get("/users/me/salary", response_model=schemas.Salary)
def read_users_me_salary(current_user: schemas.User = Depends(auth.get_current_user), db: Session = Depends(get_db)):
    salary = crud.get_salary(db, current_user.id)
    if not salary:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Salary not found",
        )
    return salary

# if __name__ == "__main__":
#     uvicorn.run("main:app", reload=True)