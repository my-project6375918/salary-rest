from datetime import date
from pydantic import BaseModel

class UserBase(BaseModel):
    username: str
    full_name: str

class UserCreate(UserBase):
    password: str

class User(UserBase):


    class Config:
        from_attributes = True

class Salary(BaseModel):
    salary_size: int
    date_increase: date

class SalaryCreate(Salary):
    user_id: int

class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None