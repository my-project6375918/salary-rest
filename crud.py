from sqlalchemy.orm import Session

import models
import schemas
from auth import get_hashed_password

def create_user(db:Session, user: schemas.UserCreate):
    password = get_hashed_password(user.password)
    db_user = models.User(hashed_password=password, username=user.username, full_name=user.full_name)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    return db_user

def get_user(db:Session, id: int): #поиск по id
    user = db.query(models.User).filter(models.User.id == id).first()
    return user

def get_username(db:Session, username:str): #поиск по никнейму
    return db.query(models.User).filter(models.User.username==username).first()

def create_salary(db: Session, salary: schemas.SalaryCreate):
    db_salary = models.Salary(salary_size=salary.salary_size, date_increase=salary.date_increase, user_id=salary.user_id)
    db.add(db_salary)
    db.commit()
    db.refresh(db_salary)

    user = get_user(db, salary.user_id)
    if user:
        user.salary_id = db_salary.salary_id
        db.commit()


    return db_salary

def get_salary(db:Session, id: int):
    return db.query(models.Salary).filter(models.Salary.user_id==id).first()
