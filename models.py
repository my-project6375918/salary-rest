from sqlalchemy import Integer, Column, String, Date, ForeignKey
from sqlalchemy.orm import relationship

from database import Base

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    username = Column(String, nullable=False)
    full_name = Column(String, nullable=False)
    hashed_password = Column(String)
    salary_id = Column(Integer, ForeignKey('salary.salary_id'), unique=True)
    salary = relationship('Salary', backref='user', uselist=False, foreign_keys=[salary_id])


class Salary(Base):
    __tablename__ = 'salary'
    salary_id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    salary_size = Column(Integer)
    date_increase = Column(Date)
    user_id = Column(Integer, ForeignKey('users.id'))
