# salary-REST
REST-сервис для просмотра текущей зарплаты и даты следующего
повышения.

## Установка
1. Клонирование репозитория
``` git clone https://gitlab.com/my-project6375918/salary-rest.git ```
2. Установка зависимостей
```pip install -r requirements.txt```
3. Запуск сервера
``` uvicorn main:app --reload```
4. Получение сведений о зарплате 
   - Авторизация пользователя:


    ![Alt-Скриншот авторизации](https://sun9-12.userapi.com/impg/aBvM6dExY1MDOoYDpJCOBqICEo7BoDEgDvTsJQ/DWx8TVHXMis.jpg?size=809x725&quality=96&sign=563eb8ea2263ff14543a3501eba8b2d9&type=album)
   - Просмотр зарплаты и даты повышения:

   
    ![Alt-Скриншот данных о зарплате](https://sun9-49.userapi.com/impg/BXpi8C_Zoc-exRsmxybYLamBFKNOW52qiALg8w/-liyel-8-_0.jpg?size=879x947&quality=96&sign=8e560dd55315abcd25fea54183e208cf&type=album)